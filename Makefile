COMPOSE_FILES=docker-compose.yml
COMPOSE_PROFILES=
COMPOSE_COMMAND=docker-compose

ifeq (, $(shell which $(COMPOSE_COMMAND)))
	COMPOSE_COMMAND=docker compose
	ifeq (, $(shell which $(COMPOSE_COMMAND)))
		$(error "No docker compose in path, consider installing docker on your machine.")
	endif
endif

ifneq (,$(wildcard ./.env))
    include .env
    export
endif

help:
	@echo "env"
	@echo "==> Create .env file"
	@echo ""
	@echo "up"
	@echo "==> Create and start containers"
	@echo ""
	@echo "provision-all"
	@echo "==> Provision dependencies for database, PHP & JS"
	@echo ""
	@echo "provision-project"
	@echo "==> Provision dependencies for both PHP & JS"
	@echo ""
	@echo "provision-db"
	@echo "==> Provision database (Migrate & Seed)"
	@echo ""
	@echo "build-up"
	@echo "==> Create and build all containers"
	@echo ""
	@echo "watch-assets"
	@echo "==> Compile assets and watch all changes"
	@echo ""
	@echo "status"
	@echo "==> Show currently running containers"
	@echo ""
	@echo "destroy"
	@echo "==> Down all the containers, keeping their data"
	@echo ""
	@echo "shell"
	@echo "==> Create an interactive shell for FPM user"
	@echo ""
	@echo "shell-as-root"
	@echo "==> Create an interactive shell for root user"
	@echo ""
	@echo "mysql-shell"
	@echo "==> Create an interactive shell for MySQL"
	@echo ""
	@echo "redis-shell"
	@echo "==> Create an interactive shell for Redis"
env:
	@[ -e ./.env ] || cp -v ./.env.example ./.env

up:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) up -d

build-up:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) up -d --build --force-recreate

build-no-cache:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) build --no-cache

status:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) ps

destroy:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) down --remove-orphans

restart: destroy up

recreate: destroy build-up

shell:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec web bash

shell-as-root:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec -u 0 web bash

mysql-shell:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec -u 0 mysql mysql -u$(DB_USERNAME) -p$(DB_PASSWORD)

provision-project:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec web /app/deploy/docker/local/provision_project.sh

provision-db:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec web /app/deploy/docker/local/provision_database.sh

provision-all: provision-project provision-db

watch-assets:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec web npm run watch
passport-install:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec web php artisan passport:install

personal-token:
	$(COMPOSE_COMMAND) -f $(COMPOSE_FILES) exec web php artisan passport:client --personal --name "User Personal Grant"
