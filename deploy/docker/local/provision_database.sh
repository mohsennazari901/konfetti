#!/usr/bin/env bash
set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "==> Finished running script, exiting"
}

echo "==> Provisioning database for local development"

php artisan migrate:fresh --seed

php artisan passport:client --personal --name "User Personal Grant"
