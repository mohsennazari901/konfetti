#!/usr/bin/env bash
set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "==> Finished running script, exiting"
}

echo "==> Provisioning for local development"

echo "==> Installing composer dependencies"

composer install

echo "==> Installing npm dependencies"

npm install

echo "==> Generating application key"
php artisan key:generate

echo "==> Generating laravel passport key"
php artisan passport:keys --force

echo "==> You'll need to migrate & seed database manually, if you think it's required"
echo "==> Or you can run make provision_db"
