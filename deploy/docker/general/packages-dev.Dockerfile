FROM konfetti/php-base:1.0.0

LABEL Version="2023-12-24"
LABEL TargetImageName="konfetti/packages-dev:1.0.0"

ENV COMPOSER_ALLOW_SUPERUSER=1

USER root
WORKDIR /app/

COPY composer.json composer.lock package.json package-lock.json /app/

RUN  set -eux; \
     composer install --no-autoloader --no-scripts --no-cache && \
     npm i
