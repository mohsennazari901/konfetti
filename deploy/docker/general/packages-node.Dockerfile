FROM node:18.13.0-bullseye

LABEL Version="2023-12-24"
LABEL TargetImageName="konfetti/packages-node:1.0.0"

USER root
WORKDIR /app/

COPY package.json package-lock.json /app/
RUN npm i
