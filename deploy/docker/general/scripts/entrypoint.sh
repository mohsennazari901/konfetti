#!/usr/bin/env bash
set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

export APP_ENV="${APP_ENV:-local}"

if [ $# -ge 1 ] && [ -n "$1" ]
then
  export CMD="$1"
else
  export CMD=$APP_CMD
fi

BOOTSTRAP_FILE=${BOOTSTRAP_FILE:-bootstrap.sh}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "==> Finished running script, exiting"
}

echo "==> Preparing project ($CMD) for ($APP_ENV) environment"

case $APP_ENV in

  production)
    DOCKER_DIR_FOR_ENV="production"
    ;;

  staging)
    DOCKER_DIR_FOR_ENV="staging"
    ;;

  *)
    DOCKER_DIR_FOR_ENV="local"
    ;;
esac

if [[ "$APP_ENV" == "production" || "$APP_ENV" == "staging" ]]; then
    echo "==> Add storage keys for passport"
    echo "$OAUTH_PUBLIC_KEY" | tr -d " \t\n\r" | base64 -d > storage/oauth-public.key
    echo "$OAUTH_PRIVATE_KEY"  | tr -d " \t\n\r" | base64 -d > storage/oauth-private.key

    echo "==> Optimize project"
    php artisan optimize
fi

if [[ "$APP_ENV" == "local" ]]; then
    echo "==> Dumping compose autoload file ..."
    composer dumpautoload

    echo "==> Clear all caches in local ..."
    php artisan optimize:clear
fi

if [ ! -f ./deploy/docker/$DOCKER_DIR_FOR_ENV/"$CMD"/"$BOOTSTRAP_FILE" ]; then
    echo "==> No boostrap file found!, running supervisord"
else
    echo "==> Found boostrap file!, bootstrapping bootstrapping './deploy/docker/$DOCKER_DIR_FOR_ENV/$CMD/$BOOTSTRAP_FILE' ..."
    bash ./deploy/docker/$DOCKER_DIR_FOR_ENV/"$CMD"/"$BOOTSTRAP_FILE"
fi
