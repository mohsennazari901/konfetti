FROM php:8.2.12-fpm-bookworm
ENV COMPOSER_VERSION=2.5.7
ENV NONROOT_USER=www-data
ENV TZ "Asia/Tehran"

LABEL Version="2023-12-24"
LABEL TargetImageName="konfetti/php-base:1.0.0"


# in case of 503 unavailable use an alternative mirror:
# https://www.debian.org/mirror/list
# for example to use one of iran mirrors:
#RUN echo 'deb https://archive.debian.petiak.ir/debian/ bookworm main' > /etc/apt/sources.list

RUN set -eux; \
    cp /usr/share/zoneinfo/${TZ} /etc/localtime && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
        git \
        curl \
        rsyslog \
        nutcracker \
        cron \
        wget \
        gnupg2 \
        ca-certificates \
        lsb-release \
        apt-transport-https \
        autoconf \
        nginx \
        supervisor \
        nodejs \
        npm \
        libzip4 \
        tzdata \
        netcat-openbsd \
        telnet \
        vim \
        tmux \
        redis-tools \
        htop \
        net-tools \
        procps \
        lsof \
        iputils-ping \
        traceroute \
        g++ \
        gcc \
        make \
        binutils \
        pkg-config \
        zlib1g-dev \
        libmcrypt-dev \
        libpng-dev \
        libxpm-dev \
        libjpeg-dev \
        libwebp-dev \
        libgmp-dev \
        libsodium-dev \
        libxml2-dev \
        libfreetype6-dev \
        libicu-dev \
        libzip-dev \
        libcurl4-openssl-dev \
        libssl-dev && \
    pecl channel-update pecl.php.net && \
    pecl install xdebug apcu mongodb redis && \
    pecl config-set php_ini "$PHP_INI_DIR" && \
    docker-php-ext-configure gd --with-freetype --with-jpeg \
        NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
    docker-php-ext-install -j$(nproc) bcmath gmp pdo_mysql soap opcache intl exif sockets gd zip && \
    docker-php-ext-enable apcu xdebug opcache mongodb zip redis && \
    apt-get -y purge \
        g++ \
        gcc \
        make \
        binutils  \
        zlib1g-dev \
        libpng-dev \
        libxpm-dev \
        libjpeg-dev \
        libwebp-dev \
        libgmp-dev \
        libsodium-dev \
        libxml2-dev \
        libfreetype6-dev \
        libicu-dev \
        libzip-dev \
        libcurl4-openssl-dev && \
    apt-get clean && \
#    Adding autoremove, removes npm and gd.so related files, God knows why :D \
#       so I will just comment it out for now.
#    apt-get autoremove --yes && \
    rm -rf /tmp/* /var/lib/apt/lists/* && \
    curl -sS https://getcomposer.org/installer | php -- \
        --version=$COMPOSER_VERSION \
        --install-dir=/usr/bin --filename=composer

RUN set -eux; \
    mkdir -p /var/log/supervisor /etc/supervisor/conf.d/ && \
    mkdir -p /var/tmp/nginx /run/nginx/ && \
    # add non-root user
    usermod -u 1000 $NONROOT_USER && groupmod -g 1000 $NONROOT_USER && \
    # give permission to required path to the generated non-root
    mkdir -p /var/run/php/ && \
    mkdir -p /var/cache/nginx/ && \
    chown -R $NONROOT_USER:$NONROOT_USER /run/ /var/ && \
    # Allow none-root user run crond
    chmod gu+s /usr/sbin/cron

CMD [ "supervisord", "-c", "/etc/supervisor/supervisord.conf" ]
