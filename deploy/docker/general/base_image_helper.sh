#!/bin/bash
set -Eeuo pipefail
IFS=$'\n\t'


PHP_BASE_TAG_VERSION="1.0.0"
PACKAGES_DEV_TAG_VERSION="1.0.0"
PACKAGES_PHP_TAG_VERSION="1.0.0"
PACKAGES_NODE_TAG_VERSION="1.0.0"

readonly REGISTRY_URL="konfetti"
readonly ACTION_BUILD="build"
readonly ACTION_PUSH="push"
readonly IMAGE_PHP_BASE="php-base"
readonly IMAGE_PACKAGES_PHP="packages-php"
readonly IMAGE_PACKAGES_NODE="packages-node"
readonly IMAGE_PACKAGES_DEV="packages-dev"
readonly IMAGE_ALL="all"


buildBasePHPImage(){
    echo "==> Building $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION ..."
    docker build --no-cache -t $REGISTRY_URL/$IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION \
        -f deploy/docker/general/php.Dockerfile .
    echo "==> $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION built successfully."
}

buildPhpPackagesImage(){
    echo "==> Building $IMAGE_PACKAGES_PHP:$PACKAGES_PHP_TAG_VERSION ..."
    docker build --no-cache -t $REGISTRY_URL/$IMAGE_PACKAGES_PHP:$PACKAGES_PHP_TAG_VERSION \
        -f deploy/docker/general/packages-php.Dockerfile .
    echo "==> $IMAGE_PACKAGES_PHP:$PACKAGES_PHP_TAG_VERSION built successfully."
}

buildNodePackagesImage(){
    echo "==> Building $IMAGE_PACKAGES_NODE:$PACKAGES_NODE_TAG_VERSION ..."
    docker build --no-cache -t $REGISTRY_URL/$IMAGE_PACKAGES_NODE:$PACKAGES_NODE_TAG_VERSION \
        -f deploy/docker/general/packages-node.Dockerfile .
    echo "==> $IMAGE_PACKAGES_NODE:$PACKAGES_NODE_TAG_VERSION built successfully."
}

buildDevPackagesImage(){
    echo "==> Building $IMAGE_PACKAGES_DEV:$PACKAGES_DEV_TAG_VERSION ..."
    docker build --no-cache -t $REGISTRY_URL/$IMAGE_PACKAGES_DEV:$PACKAGES_DEV_TAG_VERSION \
        -f deploy/docker/general/packages-dev.Dockerfile .
    echo "==> $IMAGE_PACKAGES_DEV:$PACKAGES_DEV_TAG_VERSION built successfully."
}

pushBasePHPImage(){
    echo "==> Pushing $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION ..."
    docker push $REGISTRY_URL/php-base:$PHP_BASE_TAG_VERSION
    echo "==> $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION pushed successfully."
}

pushPhpPackagesImage(){
    echo "==> Pushing $IMAGE_PACKAGES_PHP:$PACKAGES_PHP_TAG_VERSION ..."
    docker push $REGISTRY_URL/$IMAGE_PACKAGES_PHP:$PACKAGES_PHP_TAG_VERSION
    echo "==> $IMAGE_PACKAGES_PHP:$PACKAGES_PHP_TAG_VERSION pushed successfully."
}

pushNodePackagesImage(){
    echo "==> Pushing $IMAGE_PACKAGES_NODE:$PACKAGES_NODE_TAG_VERSION ..."
    docker push $REGISTRY_URL/$IMAGE_PACKAGES_NODE:$PACKAGES_NODE_TAG_VERSION
    echo "==> $IMAGE_PACKAGES_NODE:$PACKAGES_NODE_TAG_VERSION pushed successfully."
}

pushDevPackagesImage(){
    echo "==> Pushing $IMAGE_PACKAGES_DEV:$PACKAGES_DEV_TAG_VERSION ..."
    docker push $REGISTRY_URL/$IMAGE_PACKAGES_DEV:$PACKAGES_DEV_TAG_VERSION
    echo "==> $IMAGE_PACKAGES_DEV:$PACKAGES_DEV_TAG_VERSION pushed successfully."
}

# check for action & image
if [ $# -ge 2 ] && [ -n "$1" ] && [ -n "$2" ]
then
  export ACTION="$1"
  export IMAGE="$2"
else
    echo "usage: base_image_helper.sh [build | push] [php-base | packages-php | packages-node | packages-dev | packages-all | all]"
    exit
fi

# check for invalid 'action' & 'image'
if [ "${ACTION}" != "${ACTION_BUILD}" ] && [ "${ACTION}" != "${ACTION_PUSH}" ];
then
    echo "invalid action(valid: [build | push])"
    exit
fi

if [ "${IMAGE}" != "${IMAGE_PHP_BASE}" ] && [ "${IMAGE}" != "${IMAGE_PACKAGES_PHP}" ] && [ "${IMAGE}" != "${IMAGE_PACKAGES_NODE}" ] && [ "${IMAGE}" != "${IMAGE_PACKAGES_DEV}" ] && [ "${IMAGE}" != "packages-all" ] && [ "${IMAGE}" != "${IMAGE_ALL}" ];
then
    echo "invalid image(valid: [php-base | packages-php | packages-node | packages-dev | packages-all | all])"
    exit
fi


case $ACTION in
  build)
      case $IMAGE in
        $IMAGE_PHP_BASE)
            buildBasePHPImage
            ;;
        $IMAGE_PACKAGES_PHP)
            buildPhpPackagesImage
            ;;
        $IMAGE_PACKAGES_NODE)
            buildNodePackagesImage
            ;;
        $IMAGE_PACKAGES_DEV)
            buildDevPackagesImage
            ;;
        "packages-all")
            buildPhpPackagesImage
            buildNodePackagesImage
            buildDevPackagesImage
            ;;
        $IMAGE_ALL)
            buildBasePHPImage
            buildPhpPackagesImage
            buildNodePackagesImage
            buildDevPackagesImage
            ;;
      esac
    ;;

  push)
    case $IMAGE in
        $IMAGE_PHP_BASE)
            pushBasePHPImage
            ;;
        $IMAGE_PACKAGES_PHP)
            pushPhpPackagesImage
            ;;
        $IMAGE_PACKAGES_PHP)
            pushNodePackagesImage
            ;;
        $IMAGE_PACKAGES_PHP)
            pushDevPackagesImage
            ;;
        "packages-all")
            pushPhpPackagesImage
            pushNodePackagesImage
            pushDevPackagesImage
            ;;
        $IMAGE_ALL)
            pushBasePHPImage
            pushPhpPackagesImage
            pushNodePackagesImage
            pushDevPackagesImage
            ;;
      esac
esac

