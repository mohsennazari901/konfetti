# Konfetti Interview Challenge Docs
Please refer to the `docs` directory for full information about the project.

The challenge description can be found in [CHALLENGE_DESCRIPTION.md](docs/CHALLENGE_DESCRIPTION.md).

Documents include:
- [Architecture](docs/ARCHITECTURE.md): software architecture of the backend app
- [DATABASE](docs/DB_SCHEMA.png): Database Schema
- [DEVELOP_SETUP](docs/DEVELOP_SETUP.md): Local Development Setup
- [SECURITY](docs/SECURITY.md): Security measures implemented
- [TECH_STACK](docs/TECH_STACK.md): Technology stack


## Local Development
Run the command
```
make up
```
It will handle everything necessary to run the project. If you need more flexibility and option
just follow the guides in `doc/DEVELOP_SETUP.md`.
