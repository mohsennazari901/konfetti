<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Order\Enums\OrderProductStatusesEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->index()->constrained();
            $table->foreignId('event_date_id')->index()->constrained();
            $table->foreignId('product_id')->index()->constrained();
            $table->foreignId('supplier_id')->index()->constrained();
            $table->integer('final_price');
            $table->integer('quantity');
            $table->string('status')->default(OrderProductStatusesEnum::NEW->value);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_products');
    }
};
