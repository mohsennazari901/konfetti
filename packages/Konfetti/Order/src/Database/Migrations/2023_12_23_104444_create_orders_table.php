<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Order\Enums\OrderPaymentStatusesEnum;
use Konfetti\Order\Enums\OrderStatusesEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index()->constrained();
            $table->foreignId('cart_id')->index()->constrained();
            $table->string('payment_status')->default(OrderPaymentStatusesEnum::NEW->value);
            $table->string('status')->default(OrderStatusesEnum::NEW->value);
            $table->timestamp('status_changed_at')->nullable();
            $table->integer('total_items_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
