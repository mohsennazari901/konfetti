<?php

namespace Konfetti\Order\Enums;

enum OrderProductStatusesEnum: string
{
    case NEW = 'NEW';
    case CANCELED = 'CANCELED';
    case REFUNDED = 'REFUNDED';
}
