<?php

namespace Konfetti\Order\Enums;

enum OrderStatusesEnum: string
{
    case NEW = 'NEW';
    case WAITING = 'WAITING';
    case PAID = 'PAID';
    case DONE = 'DONE';
    case CANCELED = 'CANCELED';
}
