<?php

namespace Konfetti\Order\Enums;

enum OrderPaymentStatusesEnum: string
{
    case PENDING = 'PENDING';
    case NEW = 'NEW';
    case GONE_TO_GATE = 'GONE_TO_GATE';
    case CALLBACK = 'CALLBACK';
    case VERIFIED = 'VERIFIED';
    case SETTLED = 'SETTLED';
    case ACCOMPLISHED = 'ACCOMPLISHED';
    case FAILED = 'FAILED';
    case REFUND = 'REFUND';
    case CANCELED = 'CANCELED';
    case ZOMBIE = 'ZOMBIE';
}
