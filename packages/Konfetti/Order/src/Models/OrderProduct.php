<?php

namespace Konfetti\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Konfetti\Event\Models\Event;
use Konfetti\Event\Models\EventDate;
use Konfetti\Product\Models\Product;
use Konfetti\Supplier\Models\Supplier;

class OrderProduct extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'canceled_at',
        'event_id',
        'event_date_id',
        'final_price',
        'order_id',
        'product_id',
        'quantity',
        'status',
        'supplier_id',
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function eventDate(): BelongsTo
    {
        return $this->belongsTo(EventDate::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
