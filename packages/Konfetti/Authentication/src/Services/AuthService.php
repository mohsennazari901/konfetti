<?php

namespace Konfetti\Authentication\Services;

use Konfetti\Authentication\Contracts\AuthServiceContract;
use Konfetti\Authentication\Exceptions\InvalidCredentialsException;
use Konfetti\Authentication\Exceptions\UserAlreadyExistsException;
use Konfetti\Authentication\Exceptions\UserIsSuspendedException;
use Illuminate\Support\Facades\Hash;
use Konfetti\User\Models\User;

class AuthService implements AuthServiceContract
{
    /**
     * @param User $user
     * @return string
     */
    public function createToken(User $user): string
    {
        return $user->createToken('auth')->accessToken;
    }

    /**
     * @param array $data
     * @return User
     * @throws UserAlreadyExistsException
     */
    public function createNewUser(array $data): User
    {
        if (User::query()->where('email', $data['email'])->exists()) {
            throw new UserAlreadyExistsException();
        }

        $user = New User();
        $user->email = $data['email'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->password = Hash::make($data['password']);
        $user->save();

        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @return User|null
     *
     * @throws InvalidCredentialsException
     * @throws UserIsSuspendedException
     */
    public function getUserByCredentials(string $email, string $password): ?User
    {
        $user = User::query()->where('email', $email)->first();

        if (!$user || !Hash::check($password, $user->password)) {
            throw new InvalidCredentialsException();
        }

        $this->isUserSuspended($user);

        return $user;
    }

    /**
     * @param User $user
     * @return int
     */
    public function logout(User $user): int
    {
        return $user->token()->delete();
    }

    /**
     * @throws UserIsSuspendedException
     */
    private function isUserSuspended(User $user): void
    {
        if (!$user->active) {
            throw new UserIsSuspendedException();
        }
    }
}
