<?php

declare(strict_types=1);

namespace Konfetti\Authentication\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Konfetti\Authentication\Contracts\AuthServiceContract;
use Konfetti\Authentication\Services\AuthService;

class AuthenticationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->app->bind(AuthServiceContract::class, AuthService::class);
        $this->bootRateLimiters();
    }

    private function bootRateLimiters()
    {
        RateLimiter::for('authLoginLimiter', function (Request $request) {
            return Limit::perMinute(10)->by($request->user()?->id ?: $request->ip());
        });

        RateLimiter::for('authRegisterLimiter', function (Request $request) {
            return Limit::perMinute(2)->by($request->user()?->id ?: $request->ip());
        });
    }
}
