<?php

namespace Konfetti\Authentication\Exceptions;

use Konfetti\Core\Exceptions\AppException;

class UserNotLoggedInException extends AppException
{
    protected $message = "You should login to continue.";
}
