<?php

namespace Konfetti\Authentication\Exceptions;

use Konfetti\Core\Exceptions\AppException;

class UserAlreadyExistsException extends AppException
{
    protected $message = "User is Already registered.";
}
