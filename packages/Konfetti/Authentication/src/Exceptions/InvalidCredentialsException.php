<?php

namespace Konfetti\Authentication\Exceptions;

use Konfetti\Core\Exceptions\AppException;

class InvalidCredentialsException extends AppException
{
    protected $message = "The provided credentials are invalid";
}
