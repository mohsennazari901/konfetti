<?php

namespace Konfetti\Authentication\Exceptions;

use Konfetti\Core\Exceptions\AppException;

class UserIsSuspendedException extends AppException
{
    protected $message = "User is suspended";
}
