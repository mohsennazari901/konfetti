<?php

namespace Konfetti\Authentication\Contracts;

use Konfetti\Authentication\Exceptions\InvalidCredentialsException;
use Konfetti\Authentication\Exceptions\UserIsSuspendedException;
use Konfetti\User\Models\User;

interface AuthServiceContract
{
    /**
     * @param User $user
     * @return string
     */
    public function createToken(User $user): string;

    /**
     * @param array $data
     * @return User
     */
    public function createNewUser(array $data): User;

    /**
     * @param string $email
     * @param string $password
     * @return User|null
     *
     * @throws InvalidCredentialsException
     * @throws UserIsSuspendedException
     */
    public function getUserByCredentials(string $email, string $password): ?User;

    /**
     * @param User $user
     * @return int
     */
    public function logout(User $user): int;
}
