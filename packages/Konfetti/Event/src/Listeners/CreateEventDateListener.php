<?php

namespace Konfetti\Event\Listeners;

use Carbon\Carbon;
use Konfetti\Event\Contracts\EventDateServiceContract;
use Konfetti\Event\Enums\EventDateStatusesEnum;
use Konfetti\Event\Events\EventDateRequestApproved;

class CreateEventDateListener
{
    /**
     * Create the event listener.
     */
    public function __construct(public EventDateServiceContract $eventDateService)
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(EventDateRequestApproved $appEvent): void
    {
        $eventDateRequest = $appEvent->eventDateRequest;
        $eventDateRequestDates = $eventDateRequest->dates;
        $event = $eventDateRequest->event;

        $data = [
            'date_type' => $eventDateRequest->date_type,
            'duration' => $event->default_duration,
            'status' => EventDateStatusesEnum::OPEN->value,
        ];

        foreach($eventDateRequestDates as $eventDateRequestDate) {
            $start = Carbon::parse($eventDateRequestDate->selected_date_time);
            $end = $start->copy()->addMinutes($data['duration']);

            $eventDateData = array_merge($data, [
                'start' => $start,
                'end' => $end,
            ]);

            $this->eventDateService->store($event->id, $eventDateData);
        }
    }
}
