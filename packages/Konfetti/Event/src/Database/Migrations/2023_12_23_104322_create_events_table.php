<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Event\Enums\EventStatusesEnum;
use Konfetti\Event\Enums\EventTypesEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->foreignId('address_id')->index()->constrained();
            $table->boolean('approved')->default(0);
            $table->text('covid_19_summary')->nullable();
            $table->integer('default_duration');
            $table->integer('default_price');
            $table->text('description');
            $table->text('description_summary');
            $table->boolean('is_team_event')->default(0);
            $table->text('location_summary')->nullable();
            $table->timestamp('max_date')->nullable();
            $table->integer('max_duration')->nullable();
            $table->integer('max_price')->nullable();
            $table->text('meta_description')->nullable();
            $table->timestamp('min_date')->nullable();
            $table->integer('min_duration')->nullable();
            $table->integer('min_price')->nullable();
            $table->string('permalink')->index();
            $table->text('private_event_description')->nullable();
            $table->boolean('request_active')->default(1);
            $table->boolean('request_booking_solution')->default(0);
            $table->integer('request_max_tickets');
            $table->string('request_private')->default(1);
            $table->boolean('required_address')->default(0);
            $table->string('status')->default(EventStatusesEnum::OPEN->value);
            $table->text('subtitle')->nullable();
            $table->foreignId('supplier_id')->index()->constrained();
            $table->text('team_events_and_gift_cards_summary');
            $table->string('timezone')->nullable();
            $table->string('title');
            $table->string('type')->default(EventTypesEnum::IN_PERSON->value);
            $table->boolean('waitlist_active')->default(0);
            $table->text('what_is_included')->nullable();
            $table->text('what_is_included_summary')->nullable();
            $table->text('what_to_bring')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('events');
    }
};
