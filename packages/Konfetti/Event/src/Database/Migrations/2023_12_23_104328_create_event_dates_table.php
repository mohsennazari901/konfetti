<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Event\Enums\EventDateStatusesEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('event_dates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->index()->constrained();
            $table->string('date_type');
            $table->integer('duration');
            $table->timestamp('end');
            $table->integer('private_max_tickets')->default(0);
            $table->integer('private_min_tickets')->default(0);
            $table->timestamp('start');
            $table->string('status')->default(EventDateStatusesEnum::OPEN->value);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('event_dates');
    }
};
