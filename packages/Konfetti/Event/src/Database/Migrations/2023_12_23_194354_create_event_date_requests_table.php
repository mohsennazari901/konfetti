<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Event\Enums\EventDateRequestStatusEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('event_date_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->index()->constrained();
            $table->foreignId('user_id')->index()->constrained();
            $table->timestamp('user_confirmed_at')->nullable();
            $table->string('date_type');
            $table->unsignedInteger('number_of_tickets');
            $table->timestamp('supplier_decided_at')->nullable();
            $table->string('status')->default(EventDateRequestStatusEnum::CREATED->value);
            $table->string('confirmation_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('event_date_requests');
    }
};
