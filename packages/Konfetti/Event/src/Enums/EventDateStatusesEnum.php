<?php

namespace Konfetti\Event\Enums;

enum EventDateStatusesEnum: string
{
    case OPEN = 'OPEN';
    case SOLD_OUT = 'SOLD_OUT';
}
