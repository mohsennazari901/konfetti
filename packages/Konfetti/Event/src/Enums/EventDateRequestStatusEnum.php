<?php

namespace Konfetti\Event\Enums;

enum EventDateRequestStatusEnum: string
{
    case CREATED = 'CREATED';
    case USER_CONFIRMED = 'USER_CONFIRMED';
    case APPROVED = 'APPROVED';
    case REJECTED = 'REJECTED';
}
