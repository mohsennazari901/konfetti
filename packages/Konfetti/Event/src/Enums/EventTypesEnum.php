<?php

namespace Konfetti\Event\Enums;

enum EventTypesEnum: string
{
    case IN_PERSON = 'IN_PERSON';
    case ONLINE = 'ONLINE';
}
