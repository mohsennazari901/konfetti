<?php

namespace Konfetti\Event\Enums;

enum EventStatusesEnum: string
{
    case OPEN = 'OPEN';
    case SOLD_OUT = 'SOLD_OUT';
}
