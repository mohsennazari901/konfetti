<?php

namespace Konfetti\Event\Enums;

enum EventDateTypesEnum: string
{
    case PUBLIC = 'PUBLIC';
    case PRIVATE = 'PRIVATE';
    case PUBLIC_PRIVATE = 'PUBLIC_PRIVATE';

    public static function values(): array
    {
        return [
            self::PRIVATE,
            self::PUBLIC,
            self::PUBLIC_PRIVATE,
        ];
    }
}
