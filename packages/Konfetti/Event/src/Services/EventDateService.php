<?php

namespace Konfetti\Event\Services;


use Konfetti\Event\Contracts\EventDateServiceContract;
use Konfetti\Event\Enums\EventDateStatusesEnum;
use Konfetti\Event\Models\Event;
use Konfetti\Event\Models\EventDate;

class EventDateService implements EventDateServiceContract
{
    public function store(int $eventId, array $data)
    {
        $event = Event::findOrFail($eventId);

        $eventDate = new EventDate();
        $eventDate->date_type = $data['date_type'];
        $eventDate->duration = $data['duration'];
        $eventDate->start = $data['start'];
        $eventDate->end = $data['end'];
        $eventDate->status = EventDateStatusesEnum::OPEN->value;

        $event->eventDates()->save();
    }
}
