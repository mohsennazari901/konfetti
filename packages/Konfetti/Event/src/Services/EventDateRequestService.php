<?php

namespace Konfetti\Event\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Konfetti\Event\Contracts\EventDateRequestServiceContract;
use Konfetti\Event\Enums\EventDateRequestStatusEnum;
use Konfetti\Event\Events\EventDateRequestApproved;
use Konfetti\Event\Models\EventDateRequest;
use Konfetti\Event\Models\EventDateRequestDate;
use Konfetti\Supplier\Contracts\SupplierServiceContract;

class EventDateRequestService implements EventDateRequestServiceContract
{
    public function __construct(protected SupplierServiceContract $supplierService)
    {
    }

    public function store(array $data)
    {
        $eventDateRequest = new EventDateRequest();
        $eventDateRequest->date_type = $data['date_type'];
        $eventDateRequest->event_id = $data['event_id'];
        $eventDateRequest->number_of_tickets = $data['number_of_tickets'];
        $eventDateRequest->user_id = auth()->id();
        $eventDateRequest->confirmation_token = Str::random(200);
        $eventDateRequest->save();

        $selectedDates = [];
        foreach ($data['dates']  as $selectedDateTime) {
            $selectedDates[] = new EventDateRequestDate(['selected_date_time' => $selectedDateTime]);
        }
        $eventDateRequest->dates()->saveMany($selectedDates);

        return $eventDateRequest;
    }

    public function findById(int $eventDateRequestId): EventDateRequest
    {
        return EventDateRequest::find($eventDateRequestId);
    }

    public function findByToken(string $token): EventDateRequest
    {
        return EventDateRequest::query()
            ->where('confirmation_token', $token)
            ->first();
    }

    public function confirm(int $eventDateRequestId): void
    {
        if(!$eventDateRequest = EventDateRequest::find($eventDateRequestId)) {
            throw new AuthorizationException();
        }

        $eventDateRequest->status = EventDateRequestStatusEnum::USER_CONFIRMED->value;
        $eventDateRequest->user_confirmed_at = now();
        $eventDateRequest->save();
    }

    public function approve(int $eventDateRequestId): void
    {
        if(!$eventDateRequest = $this->findById($eventDateRequestId)) {
            throw new AuthorizationException();
        }

        if ($eventDateRequest->status == EventDateRequestStatusEnum::APPROVED->value) {
            return;
        }

        if ($eventDateRequest->status == EventDateRequestStatusEnum::CREATED->value) {
            throw new AuthorizationException();
        }

        $this->updateEventDateRequestStatus($eventDateRequest, EventDateRequestStatusEnum::APPROVED);

        Event::dispatch(new EventDateRequestApproved($eventDateRequest));
    }

    public function reject(int $eventDateRequestId): void
    {
        if(!$eventDateRequest = $this->findById($eventDateRequestId)) {
            throw new AuthorizationException();
        }
        if ($eventDateRequest->status == EventDateRequestStatusEnum::CREATED->value) {
            throw new AuthorizationException();
        }

        $this->updateEventDateRequestStatus($eventDateRequest, EventDateRequestStatusEnum::REJECTED);
    }

    private function updateEventDateRequestStatus(EventDateRequest $eventDateRequest, EventDateRequestStatusEnum $status): void
    {
        if (!$this->supplierService->isUserManager(auth()->id(), $eventDateRequest->supplier_id)) {
            throw new AuthorizationException();
        }

        $eventDateRequest->status = $status->value;
        $eventDateRequest->supplier_decided_at = now();
        $eventDateRequest->save();
    }
}
