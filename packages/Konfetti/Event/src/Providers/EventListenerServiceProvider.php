<?php

namespace Konfetti\Event\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Konfetti\Event\Events\EventDateRequestApproved;
use Konfetti\Event\Listeners\CreateEventDateListener;

class EventListenerServiceProvider extends ServiceProvider
{
    protected $listen = [
        EventDateRequestApproved::class  => [
            CreateEventDateListener::class,
        ],
    ];
}
