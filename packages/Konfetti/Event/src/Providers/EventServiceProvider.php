<?php

declare(strict_types=1);

namespace Konfetti\Event\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Konfetti\Event\Contracts\EventDateRequestServiceContract;
use Konfetti\Event\Contracts\EventDateServiceContract;
use Konfetti\Event\Models\EventDateRequest;
use Konfetti\Event\Observers\EventDateRequestObserver;
use Konfetti\Event\Services\EventDateRequestService;
use Konfetti\Event\Services\EventDateService;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadBindings();
        $this->bootRateLimiters();
        EventDateRequest::observe(EventDateRequestObserver::class);
    }

    public function register()
    {
        $this->app->register(EventListenerServiceProvider::class);
    }

    private function loadBindings()
    {
        $this->app->bind(EventDateServiceContract::class, EventDateService::class);
        $this->app->bind(EventDateRequestServiceContract::class, EventDateRequestService::class);
    }

    private function bootRateLimiters()
    {
        RateLimiter::for('eventDateRequestLimiter', function (Request $request) {
            return Limit::perMinute(2)->by($request->user()?->id ?: $request->ip());
        });
    }
}
