<?php

namespace Konfetti\Event\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Konfetti\Area\Models\Area;
use Konfetti\Category\Models\Category;
use Konfetti\Supplier\Models\Supplier;
use Konfetti\Tag\Models\Tag;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'address_id',
        'approved',
        'covid_19_summary',
        'default_duration',
        'default_price',
        'description',
        'description_summary',
        'is_team_event',
        'location_summary',
        'max_date',
        'max_duration',
        'max_price',
        'meta_description',
        'min_date',
        'min_duration',
        'min_price',
        'permalink',
        'private_event_description',
        'request_active',
        'request_booking_solution',
        'request_days',
        'request_max_tickets',
        'request_private',
        'required_address',
        'status',
        'subtitle',
        'supplier_id',
        'team_events_and_gift_cards_summary',
        'timezone',
        'title',
        'type',
        'waitlist_active',
        'what_is_included',
        'what_is_included_summary',
        'what_to_bring',
    ];

    public function address(): HasOne
    {
        return $this->hasOne(Area::class);
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function eventDates()
    {
        return $this->hasMany(EventDate::class);
    }
}
