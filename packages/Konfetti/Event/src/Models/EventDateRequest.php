<?php

namespace Konfetti\Event\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Konfetti\User\Models\User;

class EventDateRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'date_type',
        'event_id',
        'number_of_tickets',
        'status',
        'supplier_decided_at',
        'user_id',
        'user_confirmed_at',
        'confirmation_token',
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function dates(): HasMany
    {
        return $this->hasMany(EventDateRequestDate::class);
    }
}
