<?php

namespace Konfetti\Event\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Konfetti\User\Models\User;

class EventDateRequestDate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'event_date_request_id',
        'selected_date_time',
    ];

    public function eventDateRequest(): BelongsTo
    {
        return $this->belongsTo(EventDateRequest::class);
    }
}
