<?php

namespace Konfetti\Event\Observers;


use Konfetti\Event\Models\EventDateRequest;

class EventDateRequestObserver
{
    /**
     * Handle the EventDateRequest "created" event.
     */
    public function created(EventDateRequest $eventDateRequest): void
    {
        // Todo:
        //      1- Send mail to the user's email with the confirmation token
    }
}
