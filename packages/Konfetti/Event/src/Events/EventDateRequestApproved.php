<?php

namespace Konfetti\Event\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Konfetti\Event\Models\EventDateRequest;

class EventDateRequestApproved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(public EventDateRequest $eventDateRequest)
    {
    }
}
