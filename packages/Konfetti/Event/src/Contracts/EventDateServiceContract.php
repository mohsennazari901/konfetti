<?php

namespace Konfetti\Event\Contracts;

interface EventDateServiceContract
{
    public function store(int $eventId, array $data);
}
