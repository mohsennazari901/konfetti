<?php

namespace Konfetti\Event\Contracts;

interface EventDateRequestServiceContract
{
    public function store(array $data);

    public function confirm(int $eventDateRequestId);

    public function approve(int $eventDateRequestId);

    public function reject(int $eventDateRequestId);
}
