<?php

namespace Konfetti\Category\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Konfetti\Event\Models\Event;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'description',
        'featured',
        'name',
        'seo_meta_description',
        'seo_title',
        'showcase_title',
        'slug',
        'subtitle',
        'subtitle_gift_block',
        'title',
        'title_gift_block',
    ];

    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class);
    }
}
