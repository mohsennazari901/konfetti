<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->text('description');
            $table->boolean('featured')->default(0);
            $table->string('name');
            $table->text('seo_meta_description')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('showcase_title')->nullable();
            $table->string('slug')->unique()->index();
            $table->text('subtitle');
            $table->text('subtitle_gift_block')->nullable();
            $table->string('title');
            $table->string('title_gift_block')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
