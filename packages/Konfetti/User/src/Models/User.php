<?php

namespace Konfetti\User\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'first_name',
        'gender',
        'last_name',
        'locale',
        'newsletter',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * This appends database column
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'balance',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Get the user full name.
     *
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return ucfirst($this->first_name).' '.ucfirst($this->last_name);
    }

    /**
     * Get the user Balance.
     *
     * @return string
     */
    public function getBalanceAttribute(): string
    {
        return 0;
    }

    public function customer(): HasOne
    {
        return $this->hasOne(Customer::class);
    }

    public function managedSuppliers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'supplier_managers', 'user_id', 'supplier_id');
    }
}
