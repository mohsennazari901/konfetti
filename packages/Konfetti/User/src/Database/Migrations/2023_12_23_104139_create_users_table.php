<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Core\Enums\AppLocaleEnum;
use Konfetti\User\Enums\UserEmailStatusEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->float('balance')->default(0);
            $table->string('email')->unique()->index();
            $table->boolean('email_blacklist')->default(0);
            $table->timestamp('email_blacklist_added_at')->nullable();
            $table->string('email_status')->default(UserEmailStatusEnum::VALID->value);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('first_name');
            $table->string('gender')->nullable();
            $table->boolean('is_admin')->default(0);
            $table->string('last_name');
            $table->string('locale')->default(AppLocaleEnum::DE_DE->value);
            $table->boolean('newsletter')->default(0);
            $table->string('password');
            $table->boolean('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
