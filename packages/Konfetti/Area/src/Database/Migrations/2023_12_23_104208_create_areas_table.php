<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Area\Enums\AreaTypesEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->boolean('featured')->default(0);
            $table->integer('level')->default(1);
            $table->string('name');
            $table->string('slug')->index();
            $table->string('type')->default(AreaTypesEnum::CITY->value);
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('areas');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('areas');
    }
};
