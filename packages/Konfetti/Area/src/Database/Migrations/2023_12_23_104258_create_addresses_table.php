<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('area_id')->index()->nullable();
            $table->unsignedBigInteger('city_id')->index();
            $table->unsignedBigInteger('country_id')->index();
            $table->foreignId('user_id')->index()->constrained();
            $table->string('address');
            $table->string('house_number')->nullable();
            $table->string('house_unit')->nullable();
            $table->float('latitude');
            $table->float('longitude');
            $table->string('phone');
            $table->string('zipcode')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('city_id')->references('id')->on('areas');
            $table->foreign('country_id')->references('id')->on('areas');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('addresses');
    }
};
