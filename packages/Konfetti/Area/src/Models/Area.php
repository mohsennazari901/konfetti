<?php

namespace Konfetti\Area\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Konfetti\Area\Enums\AreaTypesEnum;

/**
 * @property string $title
 * @method Builder city()
 */
class Area extends Model
{
    protected $fillable = [
        'featured',
        'parent_id',
        'level',
        'name',
        'slug',
        'type',
    ];
    protected $hidden = [];

    public function scopeCity(Builder $query): Builder
    {
        return $query->where('type', AreaTypesEnum::CITY->value)->orderBy('order');
    }

    public function parentArea(): BelongsTo
    {
        return $this->belongsTo(__CLASS__, 'parent_id', 'id');
    }

    public function childAreas(): HasMany
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id');
    }
}
