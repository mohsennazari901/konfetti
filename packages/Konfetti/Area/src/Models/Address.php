<?php

namespace Konfetti\Area\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'country_id',
        'city_id',
        'area_id',
        'address',
        'house_number',
        'house_unit',
        'latitude',
        'longitude',
        'phone',
        'zipcode',
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(Area::class, 'city_id');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Area::class, 'country_id');
    }

    public function area(): BelongsTo
    {
        return $this->belongsTo(Area::class, 'area_id');
    }
}
