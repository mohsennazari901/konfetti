<?php

namespace Konfetti\Area\Enums;

enum AreaTypesEnum: string
{
    case COUNTRY = 'COUNTRY';
    case CITY = 'CITY';
    case AREA = 'AREA';
}
