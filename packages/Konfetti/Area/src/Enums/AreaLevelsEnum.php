<?php

namespace Konfetti\Area\Enums;

enum AreaLevelsEnum: int
{
    case ONE = 1;
    case TWO = 2;
    case THREE = 3;
}
