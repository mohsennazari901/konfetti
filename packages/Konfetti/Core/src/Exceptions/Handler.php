<?php

namespace Konfetti\Core\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $e)
    {
        parent::report($e);

        // I will comment this since sentry is not enabled.
        if (!$this->shouldntReport($e)) {
//            if (app()->bound('sentry')) {
//                app('sentry')->captureException($exception);
//            }
        }
    }

    public function render($request, Throwable $e)
    {
        if($e instanceof AppException){
            return response()->json([
                "code" => 'app-400',
                "message" => $e->getMessage(),
                "type" => "AppException",
                "detail" => null
            ], 400);
        }
        if($e instanceof AuthorizationException) {
            return response()->json([
                "code" => 'not-authorized-401',
                "message" => $e->getMessage(),
                "type" => "NotAuthorized",
            ], 401);
        }
        if ($e instanceof AccessDeniedHttpException){
            return response()->json([
                "code" => 'auth-0043',
                "message" => "This action is unauthorized.",
                "type" => "AccessDeniedException",
                "detail" => 'Ensure you have proper access or contact your admin to get access.'
            ], 403);
        }
        if($e instanceof ModelNotFoundException) {
            return response()->json([
                "code" => 'not-found-404',
                "message" => 'Entity could not be found.',
                "type" => "NotFoundException",
            ], 404);
        }
        if($e instanceof ValidationException){
            return response()->json([
                "code" => 'validation-422',
                "message" => $e->getMessage(),
                "type" => "ValidationException",
                "detail" => $e->errors()
            ], 422);
        }
        if($e instanceof TooManyRequestsHttpException) {
            return response()->json([
                "code" => 'many-request-0429',
                "message" => $e->getMessage(),
                "type" => "TooManyException",
            ], 429);
        }

        // If nothing above matches and an Accept JSON request was sent.
        if ($request->acceptsJson()) {
            return response()->json([
                "code" => 'error',
                "message" => $e->getMessage(),
                "type" => "General Exception",
            ], 400);
        }

        return parent::render($request, $e); // TODO: Change the autogenerated stub
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
