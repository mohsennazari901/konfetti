<?php

namespace Konfetti\Core\Exceptions;

use Exception;

/**
 * AppException.
 * App base Exception class. All the exception inside services should extend it.
 */
class AppException extends Exception
{

}
