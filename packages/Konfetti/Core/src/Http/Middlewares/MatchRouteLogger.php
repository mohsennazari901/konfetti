<?php

namespace Konfetti\Core\Http\Middlewares;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class MatchRouteLogger
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (\Illuminate\Http\Response|RedirectResponse) $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $response = $next($request);

        if (is_null($request->route())) {
            Log::info("MATCHED_ROUTE_404:" . $request->getMethod() . ':' .$request->path());
        } else {
            Log::info("MATCHED_ROUTE:" . $request->getMethod() . ":" . $request->route()->uri . ':' .$request->path());
        }

        return $response;
    }
}
