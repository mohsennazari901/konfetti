<?php

declare(strict_types=1);

namespace Konfetti\Core\Providers;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;
use Konfetti\Core\Exceptions\Handler;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->app->bind(ExceptionHandler::class, Handler::class);
    }

    public function register()
    {
        $this->app->register(LogServiceProvider::class);
    }
}
