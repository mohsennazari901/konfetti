<?php

namespace Konfetti\Core\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!$trackingId = request()?->header('x-tracking-id')) {
            $trackingId = (string) Str::uuid();
        }

        if (!$trackingOrigin = request()?->header('x-tracking-origin')) {
            $trackingOrigin = app()->runningInConsole() ? 'core-console' : 'core-web';
        }

        if (!$trackingVia = request()?->header('x-tracking-via')) {
            $trackingVia = app()->runningInConsole() ? 'core-console' : 'core-web';
        }

        if (!$trackingUuid = request()?->header('x-tracking-uuid')) {
            $trackingUuid = 'UUID_NOT_SET';
        }

        $this->app->singleton('tracking_id', fn () => $trackingId);
        $this->app->singleton('tracking_origin', fn () => $trackingOrigin);
        $this->app->singleton('tracking_via', fn () => $trackingVia);
        $this->app->singleton('tracking_uuid', fn () => $trackingUuid);

        Log::shareContext([
            'tracking_id'  => $this->app->make('tracking_id'),
            'tracking_origin' => $this->app->make('tracking_origin'),
            'tracking_via' => $this->app->make('tracking_via'),
            'tracking_uuid' => $this->app->make('tracking_uuid'),
        ]);
    }
}
