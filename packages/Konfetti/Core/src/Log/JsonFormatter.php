<?php

namespace Konfetti\Core\Log;

use Monolog\Formatter\JsonFormatter as BaseFormatter;

class JsonFormatter extends BaseFormatter
{
    public function __construct()
    {
        BaseFormatter::__construct(
            BaseFormatter::BATCH_MODE_JSON,
            true,
            false,
            true
        );
    }
}
