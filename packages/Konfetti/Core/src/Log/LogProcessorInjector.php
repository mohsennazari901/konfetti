<?php


namespace Konfetti\Core\Log;

use Monolog\Processor\WebProcessor;

class LogProcessorInjector
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        if (php_sapi_name() !== 'cli') {
            $logger->pushProcessor(new WebProcessor());
        }
    }
}
