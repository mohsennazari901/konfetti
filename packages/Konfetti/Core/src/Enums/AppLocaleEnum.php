<?php

namespace Konfetti\Core\Enums;

enum AppLocaleEnum: string
{
    case EN_US = 'en_US';
    case DE_DE = 'de_DE';
}
