<?php

declare(strict_types=1);

namespace Konfetti\RestApi\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class RestApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mapApiRoutes();

        $this->registerServices();
    }

    /**
     * Define the "api" routes for the application.
     *
     * @return void
     */
    protected function mapApiRoutes(): void
    {
        Route::prefix('/')->middleware('api')->group(__DIR__ . '/../Routes/api.php');

        Route::prefix('/')
            ->group(__DIR__ . '/../Routes/web.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    protected function registerServices(): void
    {
    }
}
