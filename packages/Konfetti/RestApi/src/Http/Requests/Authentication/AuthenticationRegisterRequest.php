<?php

namespace Konfetti\RestApi\Http\Requests\Authentication;

use Illuminate\Contracts\Validation\ValidationRule;
use Konfetti\Core\Http\Requests\Request;

class AuthenticationRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'unique:users,email', 'email'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'password' => ['required'],
        ];
    }
}
