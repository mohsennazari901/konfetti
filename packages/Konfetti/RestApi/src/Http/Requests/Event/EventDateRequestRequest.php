<?php

namespace Konfetti\RestApi\Http\Requests\Event;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Validation\Rules\In;
use Konfetti\Core\Http\Requests\Request;
use Konfetti\Event\Enums\EventDateTypesEnum;

class EventDateRequestRequest extends Request
{
    public array $decode = ['event_id'];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'date_type' => ['required', new In(EventDateTypesEnum::values())],
            'dates' => ['required', 'array'],
            'dates.*' => 'date',
            'number_of_tickets' => ['required', 'integer'],
            'event_id' => ['required']
        ];
    }
}
