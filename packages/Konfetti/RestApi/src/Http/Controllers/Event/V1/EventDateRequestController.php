<?php

namespace Konfetti\RestApi\Http\Controllers\Event\V1;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Konfetti\Event\Contracts\EventDateRequestServiceContract;
use Konfetti\RestApi\Http\Requests\Event\EventDateRequestRequest;

class EventDateRequestController extends Controller
{
    use ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        protected EventDateRequestServiceContract $eventDateRequestService,
    ) {
    }

    public function store(EventDateRequestRequest $request)
    {
        $this->eventDateRequestService->store(
            $request->only('date_type', 'dates', 'event_id', 'number_of_tickets'),
        );

        return response()->json([
            'status' => 'success',
            'message' => 'Please visit your email to confirm',
        ], 201);
    }

    public function confirm(Request $request)
    {
        $eventDateRequest = $this->eventDateRequestService->findByToken($request->token);

        $this->eventDateRequestService->confirm($eventDateRequest->id);
    }

    public function approve($id)
    {
        $this->eventDateRequestService->approve($id);
    }

    public function reject($id)
    {
        $this->eventDateRequestService->reject($id);
    }
}
