<?php

namespace Konfetti\RestApi\Http\Controllers\Authentication\V1;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Konfetti\Authentication\Contracts\AuthServiceContract;
use Konfetti\RestApi\Http\Requests\Authentication\AuthenticationLoginRequest;
use Konfetti\RestApi\Http\Requests\Authentication\AuthenticationRegisterRequest;
use Vinkla\Hashids\Facades\Hashids;

class AuthenticationController extends Controller
{
    use ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        protected AuthServiceContract $authService,
    ) {
    }

    public function register(AuthenticationRegisterRequest $request)
    {
        $this->authService->createNewUser(
            $request->only('email', 'first_name', 'last_name', 'password'),
        );

        return response()->json([
            'status' => 'success',
            'message' => 'Please visit your email to confirm',
        ], 201);
    }

    public function login(AuthenticationLoginRequest $request)
    {
        $user = $this->authService->getUserByCredentials(
            $request->input('email'),
            $request->input('password')
        );
        $token = $this->authService->createToken($user);

        $userData = $user->toArray();
        $userData['id'] = Hashids::encode($user->id);

        return response()->json([
            'user' => $userData,
            'token' => $token,
        ]);
    }
}
