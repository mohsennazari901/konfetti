<?php

use Illuminate\Support\Facades\Route;
use Konfetti\RestApi\Http\Controllers\Event\V1\EventDateRequestController;


Route::group([
    'prefix' => 'events/v1',
], function () {

    Route::group(['middleware' => ['auth:api']], function(){
        Route::post('request-date', [EventDateRequestController::class, 'store'])
            ->middleware(['throttle:eventDateRequestLimiter'])
            ->name('event.request-date.store');
        Route::post('request-date/{id}/approve', [EventDateRequestController::class, 'approve'])->name('event.request-date.approve');
        Route::post('request-date/{id}/reject', [EventDateRequestController::class, 'reject'])->name('event.request-date.reject');
    });

    Route::get('request-date/confirm', [EventDateRequestController::class, 'confirm'])->name('event.request-date.confirm');
});
