<?php

use Illuminate\Support\Facades\Route;
use Konfetti\RestApi\Http\Controllers\Authentication\V1\AuthenticationController;


Route::group([
    'prefix' => 'auth/v1'
], function () {
    Route::post('login', [AuthenticationController::class, 'login'])
        ->middleware(['throttle:authLoginLimiter'])
        ->name('auth.login');

    Route::post('register', [AuthenticationController::class, 'register'])
        ->middleware(['throttle:authRegisterLimiter'])
        ->name('auth.register');
});
