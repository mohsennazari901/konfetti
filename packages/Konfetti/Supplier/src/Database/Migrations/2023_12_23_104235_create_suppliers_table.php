<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Konfetti\Supplier\Enums\SupplierBusinessTypesEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('application_id')->index()->nullable()->constrained();
            $table->boolean('active')->default(1);
            $table->string('address');
            $table->string('business_type')->default(SupplierBusinessTypesEnum::LOCAL_BUSINESS->value);
            $table->text('description');
            $table->text('description_summary')->nullable();
            $table->string('email');
            $table->boolean('giftcard_embedded')->default(0);
            $table->string('logo')->nullable();
            $table->string('name');
            $table->string('permalink')->unique()->index();
            $table->string('phone');
            $table->string('subdomain')->nullable();
            $table->boolean('ticked_embedded')->default(0);
            $table->string('website')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('suppliers');
    }
};
