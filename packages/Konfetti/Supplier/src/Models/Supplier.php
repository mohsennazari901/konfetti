<?php

namespace Konfetti\Supplier\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Konfetti\Application\Models\Application;
use Konfetti\Area\Enums\AreaTypesEnum;
use Konfetti\Area\Models\Area;
use Konfetti\Event\Models\Event;
use Konfetti\User\Models\User;

class Supplier extends Model
{
    protected $fillable = [
        'application_id',
        'address',
        'business_type',
        'city_id',
        'description',
        'description_summary',
        'email',
        'logo',
        'name',
        'permalink',
        'phone',
        'status',
        'subdomain',
        'website',
    ];

    public function application(): BelongsTo
    {
        return $this->belongsTo(Application::class);
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(Area::class)->where('type', AreaTypesEnum::CITY);
    }

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    public function managers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'supplier_managers', 'supplier_id', 'user_id');
    }
}
