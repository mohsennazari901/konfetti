<?php

namespace Konfetti\Supplier\Contracts;

interface SupplierServiceContract
{
    public function isUserManager(int $userId, int $supplierId): bool;
}
