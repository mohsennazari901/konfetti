<?php

namespace Konfetti\Supplier\Services;

use Konfetti\Supplier\Contracts\SupplierServiceContract;
use Konfetti\Supplier\Models\Supplier;

class SupplierService implements SupplierServiceContract
{
    public function isUserManager(int $userId, int $supplierId): bool
    {
        if (!$supplier = Supplier::query()->find($supplierId)) {
            return false;
        }

        return $supplier->managers()->where('users.id', $userId)->exists();
    }
}
