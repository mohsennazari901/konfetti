<?php

namespace Konfetti\Supplier\Enums;

enum SupplierBusinessTypesEnum: string
{
    case LOCAL_BUSINESS = 'LocalBusiness';
}
