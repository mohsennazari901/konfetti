<?php

namespace Konfetti\Supplier\Enums;

enum SupplierStatusEnum: string
{
    case ACTIVE = 'ACTIVE';
    case CLOSE = 'CLOSE';
    case NEWLY_ADDED = 'NEWLY_ADDED';
    case SUSPEND = 'SUSPEND';
}
