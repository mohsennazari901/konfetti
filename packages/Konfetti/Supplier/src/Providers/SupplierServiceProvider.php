<?php

declare(strict_types=1);

namespace Konfetti\Supplier\Providers;

use Illuminate\Support\ServiceProvider;
use Konfetti\Supplier\Contracts\SupplierServiceContract;
use Konfetti\Supplier\Services\SupplierService;

class SupplierServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->app->bind(SupplierServiceContract::class, SupplierService::class);
    }
}
