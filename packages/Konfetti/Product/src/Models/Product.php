<?php

namespace Konfetti\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Konfetti\Event\Models\Event;
use Konfetti\Event\Models\EventDate;
use Konfetti\Supplier\Models\Supplier;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'active',
        'event_date_id',
        'event_id',
        'price',
        'quantity',
        'reserved_quantity',
        'supplier_id',
        'ship',
        'sku',
        'title',
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function eventDate(): BelongsTo
    {
        return $this->belongsTo(EventDate::class);
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }
}
