<?php

namespace Konfetti\Application\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Konfetti\Supplier\Models\Supplier;

/**
 * @property string $title
 * @method Builder city()
 */
class Application extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'company',
        'website',
        'email',
        'phone',
        'referral',
        'plan_type',
    ];

    public function supplier(): HasOne
    {
        return $this->hasOne(Supplier::class);
    }
}
