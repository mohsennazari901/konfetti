# Security
Several security measures are included in the project.

### Passport authentication
To access some endpoints you need authorization. These routes are:
- POST: `/events/v1/request-date`
- POST: `/events/v1/request-date/{id}/approve`
- POST: `/events/v1/request-date/{id}/reject`
for the above routes there are Authorization mechanism as well.


### Throttling
For reducing abusive users, a throttling is included with the following endpoints:
- POST: `/events/v1/request-date`
- POST: `/auth/v1/login`
- POST: `/auth/v1/register`

### Hashids
To reduce id exposure hashid mechanism in requests. The examples are:
- When user login and we return the data
- When we want to make event-date-request where `event_id` is hashed
