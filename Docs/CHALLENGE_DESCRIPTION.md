<p align="center"><a href="https://gokonfetti.com/en-us/" target="_blank"><img src="https://konfetti-storage.s3.eu-central-1.amazonaws.com/giftcard/logo.svg" width="200" alt="konfetti Logo"></a></p>

## Description
Hello there! At this stage, we want to get a sense of your technical and creative skills and will ask you to build a simple application with Laravel and a set of APIs with a couple of entities that are part of our daily vocabulary. It's open-ended specifically to see how think and what you value when implementing technical solutions.

We want you to create the following entities:
- `Customer`, the person who visits our website
  - Can book and attend several Events
- `Partner`, the company that provides a workshop or class
  - Can have multiple Events
- `Event`, a workshop or class that takes place in a location
  - Has an address
  - Can have multiple Event Dates
- `Event Date`, a bookable session for a particular Event
  - Can have multiple Customers booking it

Feel free to create more Entities, and structure them with the attributes you see fit. Feel free to get inspiration from our website.

## Feature Request!!
On top of that, we would like you to build a new feature, let's call it `Date Request`, where a Customer can request an `Event Date` to a `Partner` directly, and the partner can decide to accept it or not.

## BONUS POINTS
- Test coverage
- Security measures
- Error Handling

## Doing the task
- Start by creating a feature branch
- Commit frequently so we can follow your thought process
- When you finish, open a Pull Request and send us an email to [backend-task@gokonfetti.com](mailto:backend-task@gokonfetti.com)

We estimate that you can do this in between 4-5 hours, but you have up to 1 week to deliver from the moment the task is shared with you.

## Final notes
The project is using the latest version of Laravel until now with PHP 8.1 (minimum requirement) but feel free to use newer PHP/Framework versions. 

We hope you have fun and enjoy the task. If you have any questions feel free to send an [email](mailto:backend-task@gokonfetti.com) and we'll happily support you.
Good luck!
