# Software Architecture
The application is developed using Laravel framework as REST API.

Application is separated into several packages under `packages/Konfetti`:

### Packages
Application is segregated into several packages that can be called from anywhere in the application.
They are all inside `packages/Konfetti` and are:
- Application: Handling pre-registration application of suppliers.
- Area: Handling Addresses and Areas.
- Authentication: Handling Authentication.
- Cart: Handling user Cart.
- Category: Handling Event Categories.
- Core: Shared functionality of packages.
- Event: Handling Events, EventDate and EventDateRequest.
- Order: Handling Orders.
- Product: Handling Product, which is a sellable instance of an EventDate.
- RestApi: Handling RestAPI.
- Review: Handling Reviews.
- Supplier: Handling Suppliers (Partners).
- Tag: Handling Event Tags.
- User: Handling Users (Customers).

### General Service Architecture
Each service has the following general structure:
- Contracts: Contracts/interfaces and abstractions
- Database: Holding Migrations/Seeds/Factories
- Enums: Holding Enum types
- Events: Holding Laravel events
- Exceptions: Holding different Exceptions
- Http: Holding Controllers/Middlewares/Requests
- Listeners: Holding Laravel listeners
- Models: Holding Eloquent Models
- Observers: Holding Model Observers
- Routes: Holding requests routes
- Providers: Holding service providers
- Services: Holding services which runs the functioanlities

These are all autoloaded from `composer.json`.
