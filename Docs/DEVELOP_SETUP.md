# Installation
Clone the project and run the below commands:
### Configure environment variables
In the first step you need to configure environment variables:
```bash
make env
```
### Create & Up the containers
First create all the neccessary docker images using:
```bash
bash deploy/docker/general/base_image_helper.sh build all
```
Right after configuring envs, you need to run containers
```bash
make up
```
### Install dependencies & Seed the database
After installing dependencies, the database will get created, migrated & seeded
```bash
make provision-all
```
### Compile & Watch assets
Now, it's time to watch & compile assets
```bash
make watch-assets
```

### Code!
Everything is up, and you're ready to code
